// const str = msg.payload;
// const newmsg = msg.payload.newmsg || "";

// function getToken(sms){
//     var matches = sms.match(/\d{6,7}/);
//     //const found = matches.find(element => element.length > 8 && element.length < 14 );
//     if (matches.length)
//         return matches[0]
//     else
//         return null
// }
function parseData(data){
    //xử lí chuỗi +CMGL: 1,"REC READ","+84948252854","","22/06/23,07:37:48+28"
    var arr = data.split(",");
    if (arr.length > 0){
        const from = arr[2].split("\"").join("");
        const time = arr[4].split("\r\n")[0].split("\"").join("");
        const sms  = arr[4].split("\r\n")[1].split("\"").join("");
        const token = getToken(sms);
        return {from,time,sms,token}
    }
    return null
}

function getSMS(sms){
    var arr = sms.split("\r\n\r\n");
    var newsms_port_1 = [];
    arr.map(e=>{
        if (e.indexOf("+CMGL:")>=0){
            const new_sms = parseData(e);
            if (new_sms){
                newsms_port_1.push(new_sms)
            }
        }
    })
    return newsms_port_1;
}


if (newmsg){
    var sms_port_1 = global.get("sms_port_1") || []; // lay bien ve
    var newsms_port_1 = getSMS(newmsg);
    newsms_port_1.map(e=>{
        sms_port_1.push(e);
    })
    msg.payload ={
        "msg": "new msg incoming",
        "data": str,
        "newsms_port_1": newsms_port_1
    }
    global.set("sms_port_1",sms_port_1);
    return msg;
}