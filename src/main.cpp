// #include <stdio.h>
// #include <string.h>
#include <Arduino.h>


// Configure TinyGSM library
#define TINY_GSM_MODEM_SIM7600      // Modem is SIM800
#define TINY_GSM_RX_BUFFER   1024  // Set RX buffer to 1Kb
#include <TinyGsmClient.h>
 
#define DEBUG true
#define MODE_1A

#define MAX_BUFF_SIZE 255
#define SILENCE_TIME 50
#define TIME_OUT 3000
 
#define DTR_PIN 9
#define RI_PIN 8
#define LED_DEBUG 10

#define MODEM_TX             27
#define MODEM_RX             26
#define SMS_TARGET  "+84948252854"
 
TinyGsm modem(Serial1);
String from_usb = "";
char RX_BUFF[MAX_BUFF_SIZE];
//bool checkMoney = true;
bool checkSignal = true;
bool checkSIM = false;
bool checkSMS = false;


String sendATcmd(String command, const int timeout, boolean debug);
void resetArray(char* array, int size);
void ledSignalDebug();
void ledSimDebug();

 
void setup()
{
    pinMode(LED_DEBUG,OUTPUT);
    Serial.begin(115200);
    while (!Serial)
    {
        ; // wait for Arduino serial Monitor port to connect
    }

 
    Serial1.begin(115200, SERIAL_8N1, MODEM_RX, MODEM_TX);
    while (!Serial1)
    {
        ; // wait for SIM module to connect
    }
    
    Serial.println("4G Test Start!");
    sendATcmd("AT", 3000, DEBUG);
    
}
 
void loop()
{
    //read from serial1
    static uint8_t rxSize;
    static uint32_t lastReceive = millis();
    if (Serial1.available())
    {
        uint8_t quantity = Serial1.available();
        for (uint8_t i = 0; i < quantity; i++)
        {
            RX_BUFF[rxSize + i] = (char)Serial1.read();
        }
        rxSize += quantity;
        if (rxSize > MAX_BUFF_SIZE)
        {
            rxSize = 0;
        }
        lastReceive = millis();
    }
    else
    {   
        //neu doc xong roi
        if (rxSize != 0)
        {
            if ((millis() - lastReceive) > SILENCE_TIME)
            {
                String stringBuffer = RX_BUFF;
                /*check params*/
                int pHIndex = stringBuffer.indexOf("\r\npH\r\n"); //pH SMS command

                int eCIndex = stringBuffer.indexOf("\r\nEC\r\n"); // EC SMS command
                
                int smsIndex = stringBuffer.indexOf("+CMGL:"); //SMS command

                //int moneyIndex = stringBuffer.indexOf("AT\r\n"); //money checking

                int phoneStartIndex = stringBuffer.indexOf("\"REC UNREAD\",\"") + 14;
                int phoneEndIndex = phoneStartIndex + 12;

                int signalIndex = stringBuffer.indexOf("+CSQ:"); //signal checking

                int simIndex = stringBuffer.indexOf("+CPIN: "); //sim checking
                
                Serial.println("Start print buffer");
                Serial.println(RX_BUFF);
               
                //money checking
                // else if (moneyIndex > 0)
                // {
                //     Serial.println("handle money check command");  
                //     checkMoney = false;
                //     checkSIM = false;
                //     checkSignal = true;
                // }
                //signal checking
                if (signalIndex > 0)
                {
                    //+CSQ: 31,99
                    Serial.println("handle signal check command");
                    int start = stringBuffer.indexOf("+CSQ: ") + 6;
                    int end = start + 2;
                    if (stringBuffer.substring(start, end).toInt() < 25)
                    {
                        Serial.print("low signal: ");
                        Serial.print(stringBuffer.substring(start, end));
                        ledSignalDebug();
                    }                      
                    //checkMoney = false;
                    checkSIM = true;
                    checkSignal = false;
                    checkSMS = false;
                }
                //sim checking
                else if (simIndex > 0)
                {   //+CPIN: READY
                    Serial.println("handle sim check command");  
                    if (stringBuffer.indexOf("+CPIN: READY") < 0)
                    {
                        Serial.println("SIM not connected");
                        ledSimDebug();
                    }
                    
                    //checkMoney = true;
                    checkSIM = false;
                    checkSignal = false;
                    checkSMS = true;
                }                
                //response for arbitrary SMS
                else if(checkSMS)
                {
                    String senderPhoneNumber = stringBuffer.substring(phoneStartIndex, phoneEndIndex);
                    Serial.print("number of sender: ");
                    Serial.println(senderPhoneNumber);
                    if (pHIndex > 0)
                    {
                        Serial.println("handle pH command");
                        float ph = random(695,711)/100.0;
                        String smsMessage = "Gia tri pH hien tai: ";
                        smsMessage += ph;
                        Serial.println(smsMessage);
                        if(modem.sendSMS(senderPhoneNumber, smsMessage)){
                        Serial.println(smsMessage);
                        }
                        else{
                        Serial.println("SMS failed to send");
                        }
                        Serial.println("delete all read SMS, leaving unread SMS");
                        String sendBuff = "AT+CMGD=,1\r\n";
                        sendATcmd(sendBuff,3000,DEBUG);
                    }
                    else if (eCIndex > 0)
                    {
                        Serial.println("handle EC command");  
                    
                        float ec = random(15,18)/100.0;
                        String smsMessage = "Gia tri EC hien tai: ";
                        smsMessage += ec;
                        smsMessage += " mS/cm3";
                        Serial.println(smsMessage);

                        if(modem.sendSMS(senderPhoneNumber, smsMessage)){
                        Serial.println(smsMessage);
                        }
                        else{
                        Serial.println("SMS failed to send");
                        }
                        Serial.println("delete all read SMS, leaving unread SMS");
                        String sendBuff = "AT+CMGD=,1\r\n";
                        sendATcmd(sendBuff,3000,DEBUG); 
                    }                   
                    else if (smsIndex > 0)
                    {
                        //send response invalid syntax
                        Serial.println("handle invalid syntax SMS command");                      
                        String smsMessage = "Cu phap khong hop le, vui long thu lai";                       
                        Serial.println(smsMessage);

                        if(modem.sendSMS(senderPhoneNumber, smsMessage)){
                        Serial.println(smsMessage);
                        }
                        else{
                        Serial.println("SMS failed to send");
                        }
                        Serial.println("delete all read SMS, leaving unread SMS");    
                        String sendBuff = "AT+CMGD=,1\r\n";
                        sendATcmd(sendBuff,3000,DEBUG); 
                    }
                    checkSIM = false;
                    checkSignal = true;
                    checkSMS = false;
                }
                rxSize = 0;
                for (uint8_t i = 0; i < MAX_BUFF_SIZE; i++)
                {
                    RX_BUFF[i] = 0;
                }          
            }
        }
    }

    static unsigned long lastATSend = millis();
    if (millis() - lastATSend > TIME_OUT)
    {   
        // if (checkMoney)
        // {
        //     Serial.println("Sending AT cmd to check money");
        //     String sendBuff = "AT\r\n";
        //     sendATcmd(sendBuff,3000,DEBUG);
        // }
        if (checkSignal)
        {
            Serial.println("Sending AT cmd to check signal");
            String sendBuff = "AT+CSQ\r\n";
            sendATcmd(sendBuff,3000,DEBUG);
        }
        else if (checkSIM)
        {
            Serial.println("Sending AT cmd to check SIM");
            String sendBuff = "AT+CPIN?\r\n";
            sendATcmd(sendBuff,3000,DEBUG);
        }
        else if(checkSMS)
        {
            Serial.println("Sending AT cmd to read unread SMS");
            String sendBuff = "AT+CMGL=\"REC UNREAD\"\r\n";
            sendATcmd(sendBuff,3000,DEBUG);
        }
        lastATSend = millis();
    }
    
    
    
//     //SIM UART available thi log ra Serial Monitor
//     while (Serial1.available() > 0)
//     {   
//         Serial.write(Serial1.read());
//         yield();
//     }
//     //Serial available thi doc vao String from_usb roi gui UART qua Serial1.
//     while (Serial.available() > 0)
//     {
// #ifdef MODE_1A
//         int c = -1;
//         c = Serial.read();
//         if (c != '\n' && c != '\r')
//         {
//             from_usb += (char)c;
//         }
//         else
//         {
//             if (!from_usb.equals(""))
//             {
//                 sendATcmd(from_usb, 0, DEBUG);
//                 from_usb = "";
//             }
//         }
// #else
//         Serial1.write(Serial.read());
//         yield();
// #endif
//     }
}
 

bool moduleStateCheck()
{
    int i = 0;
    bool moduleState = false;
    for (i = 0; i < 5; i++)
    {
        String msg = String("");
        msg = sendATcmd("AT", 1000, DEBUG);
        if (msg.indexOf("OK") >= 0)
        {
            Serial.println("SIM7600 Module had turned on.");
            moduleState = true;
            return moduleState;
        }
        delay(1000);
    }
    return moduleState;
}
 



String sendATcmd(String command, const int timeout, boolean debug)
{
    String response = "";
    Serial1.println(command); //gui lenh qua UART SIM
    // long int time = millis();
    // //wait for timeout to read response from SIM
    // while ((time + timeout) > millis())
    // {
    //     while (Serial1.available())
    //     {
    //         char c = Serial1.read();
    //         response += c;
    //     }
    // }
    // if (debug)
    // {
    //     Serial.print(response); //log response ra Monitor, lúc này Serial lại available nhưng sẽ k có repsonse từ SIM
    //     //do gửi đi log repsonse chứ k phải lệnh AT
    // }
    return response;
}



//blink LED every 1s when signal is low
void ledSignalDebug()
{
    for (size_t i = 0; i < 10; i++)
    {
       digitalWrite(LED_DEBUG, LOW);
       delay(1000);
       digitalWrite(LED_DEBUG, HIGH);
       delay(1000);
    }
}

void ledMoneydebug()
{   
    //
    digitalWrite(LED_DEBUG, LOW);
}

//blink LED every 0.5s when SIM is not connect
void ledSimDebug()
{
    for (size_t i = 0; i < 10; i++)
    {
       digitalWrite(LED_DEBUG, LOW);
       delay(500);
       digitalWrite(LED_DEBUG, HIGH);
       delay(500);
    }
}

