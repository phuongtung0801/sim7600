const unsigned int MAX_MESSAGE_LENGTH = 12;

void setup() {
 Serial.begin(9600);
}

void loop() {

 //Check to see if anything is available in the serial receive buffer
 while (Serial.available() > 0)
 {
   //Create a place to hold the incoming message
   static char message[MAX_MESSAGE_LENGTH];
   static unsigned int message_pos = 0;

   //Read the next available byte in the serial receive buffer
   char inByte = Serial.read();

   //Message coming in (check not terminating character) and guard for over message size
   if ( inByte != '\n' && (message_pos - MAX_MESSAGE_LENGTH - 1) )
   {
     //Add the incoming byte to our message
     message[message_pos] = inByte;
     message_pos++;
   }
   //Full message received...
   else
   {
     //Add null character to string
     message[message_pos] = '\0';

     //Print the message (or do other things)
     Serial.println(message);

     //Reset for the next message
     message_pos = 0;
   }
 }
}





------------
// #include <stdio.h>
// #include <string.h>
#include <Arduino.h>


// Configure TinyGSM library
#define TINY_GSM_MODEM_SIM7600      // Modem is SIM800
#define TINY_GSM_RX_BUFFER   1024  // Set RX buffer to 1Kb
#include <TinyGsmClient.h>
 
#define DEBUG true
#define MODE_1A
 
#define DTR_PIN 9
#define RI_PIN 8

#define MODEM_TX             27 //xanh la
#define MODEM_RX             26 //xanh duong
 
 
String from_usb = "";

String sendATcmd(String command, const int timeout, boolean debug);
void resetArray(char* array, int size);

 
void setup()
{
    Serial.begin(115200);
    while (!Serial)
    {
        ; // wait for Arduino serial Monitor port to connect
    }

 
    Serial1.begin(115200, SERIAL_8N1, MODEM_RX, MODEM_TX);
    while (!Serial1)
    {
        ; // wait for SIM module to connect
    }
    
    Serial.println("4G Test Start!");
 
   // sendATcmd("AT+CGMM", 3000, DEBUG);
}
 
void loop()
{
    //SIM UART available thi log ra Serial Monitor
    // while (Serial1.available() > 0)
    // {
    //     Serial.write(Serial1.read());
    //     yield();
    // }
    if (Serial1.available())
    {
        static char responseBuff[255];
        static unsigned int messagePos = 0;
       
        char inByte = Serial1.read();
        if (inByte != '\n')
        {
            responseBuff[messagePos] = inByte;
            messagePos++;
        }
        if (inByte == '\n')
        {
            Serial.println(responseBuff);
            resetArray(responseBuff, 255);
            messagePos = 0;
        }        
    }
    
    static unsigned long lastCheck = millis();
    if (millis() - lastCheck > 3000)
    {   
        Serial.println("Sending AT cmd");
        String sendBuff = "AT+CMGL=\"ALL\"\r\n";
        sendATcmd(sendBuff,3000,DEBUG);
    }
    
    
    
//     //SIM UART available thi log ra Serial Monitor
//     while (Serial1.available() > 0)
//     {   
//         Serial.write(Serial1.read());
//         yield();
//     }
//     //Serial available thi doc vao String from_usb roi gui UART qua Serial1.
//     while (Serial.available() > 0)
//     {
// #ifdef MODE_1A
//         int c = -1;
//         c = Serial.read();
//         if (c != '\n' && c != '\r')
//         {
//             from_usb += (char)c;
//         }
//         else
//         {
//             if (!from_usb.equals(""))
//             {
//                 sendATcmd(from_usb, 0, DEBUG);
//                 from_usb = "";
//             }
//         }
// #else
//         Serial1.write(Serial.read());
//         yield();
// #endif
//     }


}
 

void resetArray(char* array, int size)
{
    for (size_t i = 0; i < size; i++)
    {
        array[i] = 0;
    }
} 
bool moduleStateCheck()
{
    int i = 0;
    bool moduleState = false;
    for (i = 0; i < 5; i++)
    {
        String msg = String("");
        msg = sendATcmd("AT", 1000, DEBUG);
        if (msg.indexOf("OK") >= 0)
        {
            Serial.println("SIM7600 Module had turned on.");
            moduleState = true;
            return moduleState;
        }
        delay(1000);
    }
    return moduleState;
}
 



String sendATcmd(String command, const int timeout, boolean debug)
{
    String response = "";
    // if (command.equals("1A") || command.equals("1a"))
    // {
    //     Serial.println();
    //     Serial.println("Get a 1A, input a 0x1A");
 
    //     //Serial1.write(0x1A);
    //     Serial1.write(26);
    //     Serial1.println();
    //     return "";
    // }
    // else
    // {
    //     Serial1.println(command); //gui lenh qua UART SIM
    // }
    Serial1.println(command); //gui lenh qua UART SIM
    long int time = millis();
    //wait for timeout to read response from SIM
    while ((time + timeout) > millis())
    {
        while (Serial1.available())
        {
            char c = Serial1.read();
            response += c;
        }
    }
    if (debug)
    {
        //Serial.print(response); //log response ra Monitor, lúc này Serial lại available nhưng sẽ k có repsonse từ SIM
        //do gửi đi log repsonse chứ k phải lệnh AT
    }
    return response;
}